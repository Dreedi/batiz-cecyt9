var newsApp = angular.module('newsApp', []);

newsApp.controller('mainController', function($scope, $http) {
    $http.get('http://api.batiz-cecyt9.appspot.com/news')
        .success( function (data) {
            $scope.news = data.news;
        })
        .error( function (data) {
            console.log('Error: ' + data);
        });
});
